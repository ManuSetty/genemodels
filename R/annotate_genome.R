

## Script for annotating genome

#' Annotate genome based on annotations in transcript database
#'
#' @param txdb TranscriptDb object
#' @param extend.utrs Integer representing extension of the UTRs
#' @param gene.specific Do not collapse overlapping models from different genes
#' @param strand.specific Do not collapse nodes on opposite strands
#' @param out.file Output file to save gene.models
#' @return GRanges object of the annotated genome
#' @export

#' @import BiocGenerics
#' @import GenomicRanges
#' @import IRanges
#' @exportClass AnnotatedGRanges

annotate.genome <- function (txdb, extend.utrs=NULL, use.regular.chrs=TRUE,
  gene.specific=FALSE, strand.specific=FALSE, out.file=NULL) {

  start.time <- get.time ()

  # Reset chromosomes if necessary
  if (use.regular.chrs) {
    isActiveSeq (txdb)[grep ('_', names (isActiveSeq (txdb)))] <- FALSE
  }

  ## Load all data
  show ('Loading all annotations...')
  transcripts <- transcripts (txdb, columns=c('tx_id', 'tx_name', 'gene_id'))
  transcript.names <- unique (values (transcripts)$tx_name)

  # Revmoe chrM
  transcripts <- transcripts[seqnames (transcripts) != 'chrM']
  seqlevels (transcripts) <- setdiff (seqlevels (transcripts), 'chrM')

  ## Symbol mapping from gene id
  symbol.mapping <- symbol.mapping.from.transcripts (transcripts)
  
  ## Reset metadata
  elementMetadata (transcripts) <- NULL
  elementMetadata (transcripts)$Annot  <- elementMetadata (transcripts)$Symbol <- NA

  ## Add tss for each component
  tss <- start (transcripts)
  inds <- as.logical (strand (transcripts) == '-')
  tss[inds] <- end(transcripts)[inds]
  tss <- GRanges (seqnames (transcripts), IRanges (tss, tss))

  ## Load gene components
  show (sprintf ("gene.specific set to %s", gene.specific))
  show (sprintf ("strand.specific set to %s", strand.specific))
  show ('Loading gene components...')
  time.start <- get.time ()
  component.func <- c(Cds='cdsBy', UTR5='fiveUTRsByTranscript', 
    UTR3='threeUTRsByTranscript', Intron='intronsByTranscript', NonCds='exonsBy')
  gene.components <- sapply (labels (component.func), function (x) {
    show (x); func <- match.fun (component.func[x])
    reduce.components.genelevel (func(txdb, use.names=TRUE), symbol.mapping, x,
                            transcript.names, tss) })
  
  ## Extend three UTRs
  if (!is.null (extend.utrs)) {
    show ('Extending 5\' UTRs...')
    gene.components$UTR5E <- extend.without.overlaps (gene.components$UTR5, transcripts, extend.utrs, strand.specific,
                                                      annotation='UTR5E', positive.extend.direction='5prime')
    show ('Extending 3\' UTRs...')
    gene.components$UTR3E <- extend.without.overlaps (gene.components$UTR3, append(gene.components$UTR5E, transcripts),
                                                      extend.utrs, strand.specific, annotation='UTR3E', positive.extend.direction='3prime')
  }
  time.end <- get.time ()
  show (sprintf ("Time: %.2f", (time.end - time.start)/60))

  # Index the components per gene
  gene.components <- lapply (gene.components, index.components)

  ## Strand specific
  if (!strand.specific)
    gene.components <- lapply (gene.components, function (x) {
      strand (x) <- '*'; x})

  ## Gene specific
  if (!gene.specific)
    gene.components <- lapply (gene.components, function (x) {
      reduce.overlapping.components (granges (x)) })
  
  ## Build gene models for all genes
  show ('Building gene models...')
  time.start <- get.time ()
  gene.models <- NULL
  for (component in labels (gene.components)) {
    show (component)
    if (is.null (gene.models)) {
      gene.models <- gene.components[[component]]
      next
    }
    gene.models <- c(setdiff.with.metadata (gene.components[[component]], gene.models), gene.models)
  }
  
  ## Intergenic regions by ignoring strands on gene.models
  if (class (gene.models) == 'AnnotatedGRanges')
    gene.models <- granges (gene.models)
  temp <-  gene.models
  strand (temp) <- '*'
  seq.lengths <- seqlengths (gene.models)
  genome <- GRanges (names (seq.lengths),
                     IRanges (rep (1, length (seq.lengths)),
                              seq.lengths),
                     Symbol=rep (NA, length (seq.lengths)),
                     Annot=rep ('Intergenic', length (seq.lengths)),
                     TSS=rep (NA, length (seq.lengths)),
                     Annot.Order=NA)
  intergenic <- setdiff.with.metadata (genome, temp)
  gene.models <- sort (c(gene.models, intergenic))

  
  # Reset "Overlap" annotation order
  mcols (gene.models)$Annot.Order[mcols (gene.models)$Symbol == 'Overlap'] <- NA


  if (!is.null (out.file))
    save (gene.models, file=out.file)
  end.time <- get.time ()
  show (sprintf ("Total time: %.2f", (end.time - start.time)/60))
  
  invisible (gene.models)
}

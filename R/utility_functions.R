

#' Make the metadata consistent
#'
#' @param from Source GRanges object
#' @param to Target GRanges object
#' @return Update \code{to} object which contains all metadata fields in \code{from}

update.metadata <- function (from, to) {
  different.mets <- setdiff (colnames (values (from)), colnames (values (to)))
  for (dm in different.mets)
    values (to)[[dm]] <- NA

  return (to)
}


#' Setdiff while retaining metadata
#'
#' @param x Source GRanges object
#' @param y Target GRanges object
#' @return \code{setdiff (x, y)} while retaining metadata 

setdiff.with.metadata <- function (x, y) {
  d <- setdiff (x, y)
  o <- as.matrix (findOverlaps (x, d))
  ## If overlaps were present
  if (length (d) != nrow (o))  {
    d <- pintersect (d[o[,2]], x[o[,1]])
  } else {
    d <- d[o[,2]]
  }
  elementMetadata (d) <- elementMetadata (x)[o[,1],]
  return (d)
}


#' Convert GRangesList to GRanges while retaining namings
#'
#' @param components GRangesList object
#' @param symbol.mapping Mapping from component name to gene symbol
#' @return GRanges object of all elements in \code{components} with the
#' \code{Symbol} metadata set to component names

unlist.components <- function (components, symbol.mapping=NULL) {

  ## Set component names if necessary
  if (is.null (symbol.mapping)) {
    symbol.mapping <- names (components)
    names (symbol.mapping) <- names (components)
  }
  
  component.names <- rep (names (components), times=elementLengths (components))
  components <- unname (unlist (components))
  values (components)$Symbol <- symbol.mapping[component.names]

  return (components)
}



#' Index GRangesList objects
#'
#' @param components GRangesList object
#' @return GRanges object of all elements in \code{components} with the
#' \code{Symbol} metadata set to component names and \code{Component.Index }
#' set to the strand-specific index

index.components <- function (components) {

  component.names <- mcols (components)$Symbol

  ## Index components
  strands <- as.vector (strand (components))
  inds <- unlist (tapply (1:length (components), component.names,
                          function (x) {
                            inds <- 1:length (x)
                            if (strands[x[1]] == '-')
                              inds <- rev (inds)
                            return (inds)
                          }))

  # Sort according to identifies
  id.inds <- sort (component.names, index.return=TRUE, method='shell')$ix
  components <- components[id.inds]
  mcols (components)$Annot.Order <- inds

  return (components)
}



#' Find longest transcripts
#'
#' @param transcripts Transcripts object. Unique identifier is tx_name
#' @param symbol.mapping Mapping from tx_id to gene symbol
#' @return GRanges object representing longest transcript for each gene

longest.transcripts <- function (transcripts, symbol.mapping) {

  ## Determine gene names
  refseq <- elementMetadata (transcripts)$tx_name
  symbols <- symbol.mapping[refseq]
  widths <- width (transcripts)

  ## Refseq ids witn longest length and max number of exons
  inds <- unlist (tapply (1:length (transcripts), symbols, function (x) {
    refseq[x[widths[x] == max (widths[x])]] } ))
  
  transcripts[refseq %in% inds]
}


#' Extract tx_name to gene_id mapping from transcripts

symbol.mapping.from.transcripts <- function (transcripts) {
  transcript.names <- unique (elementMetadata (transcripts)$tx_name)
  symbol.mapping <- unlist (values (transcripts)$gene_id)
  names (symbol.mapping) <- unlist (values (transcripts)$tx_name)

  return (symbol.mapping)
}


#' Get time for execution
get.time <- function ()
  as.numeric(format(Sys.time(), "%s"))

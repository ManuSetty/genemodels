\name{symbol.mapping.from.transcripts}
\alias{symbol.mapping.from.transcripts}
\title{Extract tx_name to gene_id mapping from transcripts}
\usage{
  symbol.mapping.from.transcripts(transcripts)
}
\description{
  Extract tx_name to gene_id mapping from transcripts
}

